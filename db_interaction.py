from sqlite3 import Error
import sqlite3

def create_connection():
    """ create a database connection to the SQLite database
        specified by db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect('house_lord.db')
    except Error as e:
        print(e)

    return conn

def retrieve_table(group_filter) -> list:
    """
    Retrieve all the table content
    :param None:
    :return: table_content
    """
    try:
        connection = create_connection()
        sql = "SELECT item_name FROM grocery_item WHERE telegram_group=?"
        cur = connection.cursor()
        cur.execute(sql, (group_filter,))
    except sqlite3.Error as error:
            print("Failed to insert Python variable into sqlite table", error)
    finally:
        list_to_return = {element for element in cur}
        destroy_connection(connection)
    return list_to_return

def add_item(item):
    """
    Add a new item into the items table
    :param item:
    :return: item id
    """
    try:
        connection = create_connection()
        sql = ''' INSERT INTO grocery_item(item_name,added_by,telegram_group,insert_date)
                          VALUES(?,?,?,?); '''
        cur = connection.cursor()
        cur.execute(sql, item)
        connection.commit()
        destroy_connection(connection)
    except sqlite3.Error as error:
            print("Failed to insert Python variable into sqlite table", error)
    finally:
        if (connection):
            connection.close()
    return cur.lastrowid

def remove_item(item, user_id, chat_type):
    """
    Remove an item from the items table
    :param item:
    :return: item id
    """
    try:
        connection = create_connection()
        sql = """DELETE FROM grocery_item WHERE item_name=? AND added_by=? AND telegram_group=?"""
        cur = connection.cursor()
        cur.execute(sql, (item, user_id, chat_type))
        connection.commit()
        destroy_connection(connection)
    except sqlite3.Error as error:
        print("Failed to remove item from sqlite table", error)
    finally:
        if (connection):
            connection.close()
    return cur.lastrowid


def remove_all(group_filter):
    """
    Delete the entire table content
    :param None:
    :return: table_content
    """
    try:
        connection = create_connection()
        sql = """DELETE FROM grocery_item WHERE telegram_group=?"""
        cur = connection.cursor()
        cur.execute(sql, (group_filter,))
        connection.commit()
        destroy_connection(connection)
    except sqlite3.Error as error:
            print("Failed to insert Python variable into sqlite table", error)
    return

def destroy_connection(db_connection):
    """ create a database connection to the SQLite database
        specified by db_file
    :param db_file: database file
    :return: Connection object or None
    """
    if db_connection:
        db_connection.close()
    else:
        print("The connection is not open")