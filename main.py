import locale
import logging # debug library
import os
from telegram import Update
from telegram.ext import Updater
from telegram.ext import CommandHandler
from telegram.ext import MessageHandler
from telegram.ext import CallbackContext
from telegram.ext import Filters

# Class with all the bot's methods
from calls import BotRun
#import schedule

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)

# Change output language as Italian
locale.setlocale(locale.LC_TIME, "it_IT")


def send_command(update: Update, context: CallbackContext):
    grocery_list_instance.callback(update, context)


def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, context.error)


def main(token):
    """Start the bot."""

    #schedule.every().day.at("10:30").do(BotRun.reminders, category="Affitto")

    # Create the Updater and pass it your bot's token.
    # Make sure to set use_context=True to use the new context based callbacks
    # Post version 12 this will no longer be necessary
    updater = Updater(token, use_context=True)

    # Get the dispatcher to register handlers
    dispatcher = updater.dispatcher

    # Create grocery list instance
    global grocery_list_instance
    grocery_list_instance = BotRun()

    # on different commands - answer in Telegram
    dispatcher.add_handler(CommandHandler('lista_spesa', send_command))
    dispatcher.add_handler(CommandHandler('aggiungi', send_command))
    dispatcher.add_handler(CommandHandler('rimuovi', send_command))
    dispatcher.add_handler(CommandHandler('svuota_lista', send_command))
    dispatcher.add_handler(CommandHandler('help', send_command))
    dispatcher.add_handler(CommandHandler('ciao', send_command))

    # this handler must be placed as last call, otherwise it will cover other functionalities
    dispatcher.add_handler(MessageHandler(Filters.command, send_command))

    # log all errors
    dispatcher.add_error_handler(error)

    # Start the Bot
    run(updater)

def run(updater):
    PORT = int(os.environ.get("PORT", "8443"))
    HEROKU_APP_NAME = os.environ.get("HEROKU_APP_NAME")
    # Code from https://github.com/python-telegram-bot/python-telegram-bot/wiki/Webhooks#heroku
    updater.start_webhook(listen="0.0.0.0",
                          port=PORT,
                          url_path=os.environ.get("TOKEN"))
    updater.bot.set_webhook("https://{}.herokuapp.com/{}".format(HEROKU_APP_NAME, os.environ.get("TOKEN")))

if __name__ == '__main__':
    main(os.environ.get("TOKEN"))
