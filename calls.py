from telegram import Update
from telegram import ParseMode
from telegram import InlineKeyboardButton
from telegram import InlineKeyboardMarkup
from telegram.ext import CallbackContext

# Libraries to manage automatic-date tasks
import calendar
import telegram

# Python library for dates manipulation
import datetime

# My custom library to manage grocery Lists and DB
import grocery


def keyboard_definition():
    keyboard = [[
        InlineKeyboardButton("Option 1", callback_data=str('1')),
        InlineKeyboardButton("Option 2", callback_data=str('2'))
    ]]
    return InlineKeyboardMarkup(keyboard)


class BotRun:

    def __init__(self):
        # Create grocery class instance for grocery list
        self.what_is_missing_at_home = grocery.Lst()

    def callback(self, update: Update, context: CallbackContext = ""):
        reply_markup = keyboard_definition()

        # La seguente variabile contiene il comando ricevuto in input al bot
        # che utilizzerò nell'if che segue per derminare che azione intraprendere
        command = str(update.message.text.lower())

        if command.startswith("/ciao"):
            message = BotRun.help_message()
            update.message.reply_text(message, reply_markup=reply_markup)
            return

        elif command.startswith("/help"):
            message = BotRun.help_message()
            update.message.reply_text(message, parse_mode=ParseMode.HTML)
            return

        elif command.startswith("/lista_spesa"):
            message = self.grocery_list(update)

        elif command.startswith("/svuota_lista"):
            message = self.clear_list(update)

        elif command.startswith("/aggiungi"):
            message = self.add_to_grocery(update, context)

        elif command.startswith("/rimuovi"):
            message = self.remove_from_list(update, context)

        else:
            message = "Mi dispiace, non riesco a capire il comando."
            update.message.reply_text(message, parse_mode=ParseMode.HTML)
            return

        if message:
            update.message.reply_text(message)

    def grocery_list(self, update) -> str:
        message = "Lista della spesa:\n"
        try:
            my_list = self.what_is_missing_at_home.show(update.effective_chat)
            if my_list:
                for item in my_list:
                    message += "• " + str(item[0]) + "\n"
                message += "\nPer rimuovere un oggetto dalla lista: /rimuovi_da_lista latte\n"
            else:
                message = """A casa non manca nulla! :) \n\
            Se vuoi aggiungere qualcosa digita ad esempio: \n/aggiungi_a_lista pane\n"""
        except UnboundLocalError:
            print("An error occurred during the DataBase connection.")

        return message

    def add_to_grocery(self, update, context) -> str:
        if context.args:
            result = True
            article = ""
            for word in context.args:
                result *= isinstance(word, str)
                if result:
                    article = f'{article} {word}'

            article = article.lstrip()

            if result:
                if update.effective_chat.type == "group":
                    self.what_is_missing_at_home.insert(
                        article,
                        update.effective_user,
                        update.effective_chat.id
                    )
                else:
                    chat_id = "private_" + str(update.effective_user.id)
                    self.what_is_missing_at_home.insert(
                        article,
                        update.effective_user,
                        chat_id
                    )
                return ""
            else:
                return str(context.args[0]) + """ non risulta un valido oggetto da aggiungere alla lista
        Per favore, indicami cosa aggiungere in questo modo: /aggiungi_a_lista formaggio
        mettendo al posto di "formaggio" quello che vuoi aggiungere. Solo lettere. 
        """
        else:
            return """Per aggiungere qualcosa alla lista digita, ad esempio: \n
        /aggiungi_a_lista formaggio"""

    def remove_from_list(self, update, context) -> str:
        if context.args:
            result = True
            article = ""
            for word in context.args:
                result *= isinstance(word, str)
                if result:
                    article = f'{article} {word}'

            article = article.lstrip()

            if result:
                if update.effective_chat.type == "group":
                    self.what_is_missing_at_home.remove(
                        article,
                        update.effective_user,
                        update.effective_chat.id
                    )
                else:
                    chat_id = "private_" + str(update.effective_user.id)
                    self.what_is_missing_at_home.remove(
                        article,
                        update.effective_user,
                        chat_id
                    )
                return ""
            else:
                return str(context.args[0]) + """ non risulta un valido oggetto.\n
        Per favore, indicami cosa rimuovere in questo modo: /rimuovi_da_lista latte
        mettendo al posto di "latte" quello che vuoi rimuovere. Solo lettere. 
        """
        else:
            return """Per rimuovere qualcosa dalla lista digita, ad esempio: \n/rimuovi_da_lista latte"""

    def clear_list(self, update) -> str:
        self.what_is_missing_at_home.flush(update.effective_chat)
        return "Ora la lista è vuota."

    @staticmethod
    def reminders(category: str):
        d = datetime.date.today()
        today = d.day
        month = d.month
        chat_id = -356584339
        bot = telegram.Bot(token=os.environ.get("TOKEN"))
        if category == "Natalia" and ((calendar.monthrange(int(d.strftime("%Y")), month)[1] - today) < 7):
            bot.send_message(chat_id=chat_id, text="Domani si paga Natalia!")
        if category == "Affitto" and today == 1:
            bot.send_message(chat_id=chat_id, text="E' arrivato il momento di pagare l'affitto 🙁")

    @staticmethod
    def help_message() -> str:
        return "Ecco la lista dei comandi disponibili:\n\n" \
            "<b>lista</b> - Quali prodotti mancano in casa?\n" \
            "<b>aggiungi</b> - Aggiungi un prodotto a lista della spesa\n" \
            "<b>rimuovi</b> - Rimuovi un prodotto da lista spesa\n" \
            "<b>svuota_lista</b> - Svuota completamente la lista della spesa\n" \
            "<b>help</b> - Mostra questo messaggio di aiuto"

