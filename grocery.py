from datetime import date
import db_interaction

class Lst:
    def __init__(self):
        self.lst = list()

    def __iter__(self):
        return iter(self.lst)

    def __bool__(self):
        return bool(self.lst)

    def __len__(self):
        return len(self.lst)

    def append(self, item):
        self.lst.append(str(item))

    def insert(self, item_to_buy, user_info, chat_id=None):
        # insert a new item
        if chat_id:
            item = (item_to_buy, user_info.id, chat_id, date.today())
        else:
            item = (item_to_buy, user_info.id, "private", date.today())
        item_id = db_interaction.add_item(item)
        return

    def remove(self, item_bought, user_info, group_id=None):
        # remove item from list
        if group_id:
            db_interaction.remove_item(item_bought, user_info.id, group_id)
        else:
            print("No group id identified")
        return

    def flush(self, chat_info):
        # Clear entire list
        if chat_info.type == "private":
            chat_id = "private_" + str(chat_info.id)
        else:
            chat_id = str(chat_info.id)
        return db_interaction.remove_all(chat_id)

    def show(self, chat_info):
        if chat_info.type == "private":
            chat_id = "private_" + str(chat_info.id)
        else:
            chat_id = str(chat_info.id)
        return db_interaction.retrieve_table(chat_id)
